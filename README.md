# Open Burial Map

Interactive map that shows the details of buried people based on OpenStreetMap and Wikidata.

## Used technologies

- [OpenStreetMap](https://www.openstreetmap.org/about) and its [`buried:wikidata`](https://wiki.openstreetmap.org/wiki/Key:wikidata#Secondary_Wikidata_link) tag
- [Wikidata](https://www.wikidata.org/wiki/Wikidata:Introduction), its [SPARQL Query Service](https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service) and its [P119 (place of burial)](https://www.wikidata.org/wiki/Property:P119) property
- [Overpass API](https://wiki.openstreetmap.org/wiki/Overpass_API)
- [MapLibre GL JS](https://maplibre.org/projects/maplibre-gl-js/)

This project is based on [OSM-Wikidata Map Framework](https://gitlab.com/openetymologymap/osm-wikidata-map-framework), for more details see its [README](https://gitlab.com/openetymologymap/osm-wikidata-map-framework/-/blob/main/README.md) and [CONTRIBUTING guide](https://gitlab.com/openetymologymap/osm-wikidata-map-framework/-/blob/main/CONTRIBUTING.md).

## Screenshots

Color grouping by source:
[![Color grouping by source screenshot](images/by_source.jpeg)](https://burial.dsantini.it/#2.394,48.861,15.6,source,overpass_all_wd+wd_indirect)

Color grouping by country:
[![Detail view screenshot](images/by_country.jpeg)](https://burial.dsantini.it/#2.394,48.861,15.6,country,overpass_all_wd+wd_indirect)

Detail view:
[![Detail view screenshot](images/details.jpeg)](https://burial.dsantini.it/#-77.074,38.879,14.9,country,overpass_all_wd+wd_indirect)

Cluster view:
[![Cluster view screenshot](images/clusters.jpeg)](https://burial.dsantini.it/#2.331,48.864,12.6,country,overpass_all_wd+wd_reverse)
